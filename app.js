'use strict';

function Hamburger(size, stuffing, topping) {
    this.size = size;
    this.stuffing = stuffing;
    this.topping = topping;

    /* Error: No size */
    try {
        if (size.name === 'SIZE_SMALL' || size.name === 'SIZE_LARGE') {
            this.size = size;
        } else {
            throw new Error ('No size given');
        }
    }
    catch (error) {
        throw error;
    }

    /* Error: No stuffing */
    try {
        if (stuffing.name === 'STUFFING_CHEESE' || stuffing.name === 'STUFFING_SALAD' || stuffing.name === 'STUFFING_SALAD') {
            this.stuffing = stuffing;
        } else {
           throw new Error ('No stuffing given')
        }
    }
    catch (error) {
        throw error;
    }

    /* Error: No topping */
    try {
        if (topping.name === 'TOPPING_SPICE' || topping.name === 'TOPPING_MAYO') {
            this.topping = topping;
        }
    }
    catch (error) {
        throw error;
    }
}

Hamburger.SIZE_SMALL = {
    name: 'SIZE_SMALL',
    price: 50,
    calories: 20,
};

Hamburger.SIZE_LARGE = {
    name: 'SIZE_LARGE',
    price: 100,
    calories: 40,
};

Hamburger.STUFFING_CHEESE = {
    name: 'STUFFING_CHEESE',
    price: 10,
    calories: 20,
};

Hamburger.STUFFING_SALAD = {
    name: 'STUFFING_SALAD',
    price: 20,
    calories: 5,
}

Hamburger.STUFFING_POTATO = {
    name: 'STUFFING_POTATO',
    price: 15,
    calories: 10,
}

Hamburger.TOPPING_SPICE = {
    name: 'TOPPING_SPICE',
    price: 15,
    calories: 0,
}

Hamburger.TOPPING_MAYO = {
    name: 'TOPPING_MAYO',
    price: 20,
    calories: 5,
}

Hamburger.prototype.addStuffing = function(stuffingName) {
    try {
        if (this.stuffing.includes(stuffingName)) {
            throw new Error(`duplicate topping '${stuffingName.name}'`);
        } else {
            this.stuffing.push(stuffingName);
        }
    }
    catch (error) {
        throw error;
    }
}

Hamburger.prototype.removeStuffing = function(stuffingName) {
    try {
        if (this.stuffing.includes(stuffingName)) {
            this.stuffing.splice(stuffingName.index, 1);
        }
else
    {
        throw new Error(`'${stuffingName.name}' stuffing is not added`);
    }
}
catch (error) {
    throw error;
    }
}

Hamburger.prototype.addTopping = function(toppingName){
    this.topping.push(toppingName);
    try {
        if (this.topping.includes(toppingName)) {
            throw new Error(`duplicate topping '${toppingName.name}'`);
        } else {
            this.topping.push(toppingName);
        }
    }
    catch (error) {
        throw error;
    }
}

Hamburger.prototype.removeTopping = function(toppingName) {
    try {
        if (this.topping.includes(toppingName)) {
            this.topping.splice(toppingName.index, 1);
        } else {
            throw new Error(`'${toppingName.name}' topping is not added`);
        }
    }
    catch (error) {
        throw error;
    }
}

Hamburger.prototype.getHamburgerSize = function() {
    return this.size.name;
}

Hamburger.prototype.getHamburgerStuffing = function() {
    return this.stuffing.name;
}

Hamburger.prototype.getHamburgerTopping = function() {
    return this.topping.name;
}

Hamburger.prototype.calculatePrice = function() {
    return this.size.price + this.stuffing.price + this.topping.price;
}
Hamburger.getPrice = function() {
    Hamburger.prototype.calculatePrice().result;
}

Hamburger.prototype.calculateCalories = function() {
    return this.size.calories + this.stuffing.calories + this.topping.calories;
}

let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD, Hamburger.TOPPING_SPICE);

console.log(hamburger);
hamburger.addStuffing();
hamburger.removeStuffing();
hamburger.addTopping();
hamburger.removeTopping();
hamburger.getHamburgerSize();
hamburger.getHamburgerStuffing();
hamburger.getHamburgerTopping()
hamburger.calculatePrice();
hamburger.calculateCalories();
